Title: SCM home storing comparison

## SCM home storing comparison

|                             | svn                   | git |
| --------------------------- | --------------------- | --- |
| Overlays[^overlays]         | No[^svn-overlay]      |     |
| Single-files[^single-files] | No[^svn-overlay]      |     |
| Profiles[^profiles]         | Yes[^svn-profile]     |     |
| Atomic-changesets           | Yes                   |     |
| Offline-work                | Partial[^svn-offline] |     |
| Rename-history              | Yes                   |     |

[^overlays]: Unionfs in the working copy from different repos.

[^single-files]: Allow checking out single files (needed for dot-files).

[^profiles]: To split pub from priv data, and annon and authed checkouts.

[^svn-overlay]: Could be done with [laysvn](http://laysvn.alioth.debian.org/)
    (layered subversion), from Erich Schubert.

[^svn-profiles]: Using svn:externals.

[^svn-offline]: Each wc has the pristine copy against which you can diff. For
    total offline support you would need somehting like svk.
