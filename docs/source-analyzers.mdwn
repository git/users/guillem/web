API/ABI breakage
----------------

* icheck
* abicheck

static code analyzers
---------------------

* cppcheck
* sparse

leak checkers
-------------
* valgrind
* electric-fence
* ccmalloc
* leaktracer
* libdebug0-dev
* libdmalloc-dev
* libleakbug-dev

security/bug checkers
-----------------
* pscan
* pstack
* tendra
* flawfinder
* splint

refactoring
-----------
* similarity-tester

unit testing frameworks
-----------------------
* check

other
-----
* cflow
* ncc
