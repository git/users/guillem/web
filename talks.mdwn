Title: Talks

I have given some talks on subjects I was involved in or was working on.
Some were video recorded, but I don't all are publicly available; some
should have accompanying slides. Here is a list of them:

* GNUAB and Free Software (introductory talk @
  [UAB, Barcelona, Spain](https://www.uab.cat/), 2003-06).
* Debian (introductory talk @
  VI Congreso Hispalinux, Madrid, Spain, 2003-09).
* Debian (interview @ Punt Omega,
  [Canal 33 TV](https://www.ccma.cat/tv3/programacio/canal-33/), 2004-01).
* Debian GNU/kFreeBSD port (talk @
  [PartyZIP@, Monzón, Spain, 2004-07](http://www.partyzipa.com/conferencias2004.php);
  [Jornades de Programari Lliure III, Manresa, Spain, 2004-07](http://jpl.cpl.upc.edu/iii-jornades);
  [slides](debian/talks/partyzipa/)).
* Catalan translation team coordination (@ Softcatala).
* Debian GNU/Hurd port (talk @ [Badopi](http://www.badopi.org/) on
  [Bocanord, Barcelona, Spain](https://ajuntament.barcelona.cat/), 2004-12).
* Porting to Debian GNU variants: Targeting POSIX instead of GNU/Linux
  (talk @
  [FOSDEM, Brussels, Belgium, 2005-02](https://archive.fosdem.org/2005/2005/index/dev_room_hurd.html);
  [slides](debian/talks/fosdem/gnu_porting_fosdem2005.mgp)).
* Debian (introductory talk @
  [Debconf-ES II, Guadalajara, Spain, 2005-12](https://www.debian-es.org/debconf-es2/index.html)).
* Maemo and Nokia 770 (talk @
  [UAB, Barcelona, Spain](https://www.uab.cat/), 2006-03).
* Debian (interview @ [Debconf 9](https://debconf9.debconf.org/),
  [Extremadura Radio station](https://www.canalextremadura.es/), 2009-07).
* Gurús tecnológicos (interview @ [Debconf 9](https://debconf9.debconf.org/),
  Conécta-T 45, [Canal Extremadura TV](https://www.canalextremadura.es/),
  2009-07).
* Dpkg: The Interface (talk @
  [DebConf 15, Heidelberg, Germany, 2015-08](https://debconf15.debconf.org/);
  [slides](debian/talks/dpkg-the-interface.md),
  [video](https://summit.debconf.org/debconf15/meeting/309/dpkg-the-interface/)).
