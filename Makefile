#
# personal webpage makefile
#
# Copyright © 1998-2023 Guillem Jover <guillem@hadrons.org>
#

FULGURITE_GENWEBSITE = site/bin/genwebsite
FULGURITE_GENPATCHES = site/bin/genpatches

OPENPGPKEY_NEW := guillem-4F3E74F436050C10F5696574B972BF3EA4AE57A3.asc
# Old key to be revoked eventually
OPENPGPKEY_OLD := guillem-6AAA6732BBA3157CB1522E71B96F5C899D928C9B.asc

OPENPGPKEYS := $(OPENPGPKEY_NEW) $(OPENPGPKEY_OLD)

MDWN = $(wildcard *.mdwn docs/*.mdwn debian/talks/partyzipa/*.mdwn)
HTML = $(MDWN:.mdwn=.html)
HTML += patches.html
AUTO = patches.mdwn
FEED = mind

all: build

%.html: %.mdwn .website.conf
	$(FULGURITE_GENWEBSITE) $< >$@

patches.mdwn: patches/.git/
	$(FULGURITE_GENPATCHES) patches >$@

patches.html: patches.mdwn .website.conf
	$(FULGURITE_GENWEBSITE) --no-render-mdwn $< >$@

$(FEED):
	$(FULGURITE_GENWEBSITE) --mode=feed $@

.PHONY: $(FEED)

build: $(HTML) $(FEED)

clean:
	$(RM) $(HTML) $(AUTO)
	$(RM) -r $(FEED)/tags/
	find $(FEED) -name '*.html' -o -name '*.atom' | xargs $(RM)

create:
	mkdir -p tmp
	mkdir -p src

.PHONY: all build create clean

guillem.asc: $(OPENPGPKEY_NEW)
	ln -sf $< $@

$(OPENPGPKEYS):
	gpg -a --export $(patsubst guillem-%.asc,%,$@) >$@

update: guillem.asc $(OPENPGPKEYS)

.PHONY: update guillem.asc $(OPENPGPKEYS)
