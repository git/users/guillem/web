Implementation notes (2009-08-22 v0.2)
--------------------

* Specify and print allowed architectures.

  - Add support for /etc/dpkg/dpkg.cfg.d/. (DONE)
  - New «dpkg --print-foreign-architectures» (or similar).
  - New «dpkg --foreign-architecture» (or similar, multiple calls or comma
    separated values).

* dpkg related sub-commands run from maintainers scripts might need to know
  the package architecture (dpkg-query, dpkg-trigger, etc?).

  - New DPKG_MAINTSCRIPT_ARCH env var. (DONE)
  - Or explicit <pkgname>:<arch> on command line?

* Issues when printing package information, or when specifiying package on
  the command line (admin PoV).

  - «dpkg -l», «dpkg -S», «dpkg -L» ...
  ? If (supported arches > 1 && pkg.arch == foreign)
      Print pkg:arch;
    Else
      Print pkg;
  ? Default to assume pkg:native on input.

* Compare/checksum/hash files to avoid conflicts if they match and they are
  allowed to due to Multi-Arch:same.

  - Same contents and metadata.
  - If (pkgA.name == pkgB.name &&
        pkgA.multiarch_same && pkgB.multiarch_same &&
        fileA == fileB)
      No Conflict;

* Implement Multi-Arch field parsing and dumping. (DONE)

* On-disk storage of foreign (and coinstallable) pkg data.

  - Currently only one instance of pkgname can be loaded per parsing.
  - Control files database path changes (need the arch somewhere).
  - Need an interface to get database control file paths (at least
    for non internal ones) «pu/query-control-files». (DONE)
  ? Can use opportunity to poolize /var/lib/dpkg/info dir (optimization).

* In-core storage of coinstallable pkg data.

  - Should store in a different struct data that might differ per arch
    package (pkginfoperfile → per arch?):
  - Package metadata (arch, depends, priority?).
  - File info (file paths, checksums?).

* Apply Multi-Arch semantics on dependency relationships (src/).

* Consider if libs with different ABI per arch are going to be problematic
  (libc6 vs libc6.1).

  - Should not have non-versioned files!
  - Different pkgname anyway.
  ...

