Title: Debian triplets
Status: Draft
Author: Guillem Jover <guillem@debian.org>
Thread: https://lists.debian.org/debian-arm/2011/02/msg00091.html
Last-Change: 2011-11-02


GNU triplets
------------

* Upstream claims GNU triplets do not fully encode ABI.
  - Thus cannot be used properly to map deb arch ←→ GNU triplet.
  - Thus cannot be used for multiarch paths.
  - Thus cannot be used for cross-toolchains sharing GNU triplet.
  - Thus we need yet another ABI encoding and mapping for multiarch and
    dpkg architectures, instead of being able to just use GNU triplets.
    <https://wiki.debian.org/Multiarch/Tuples>

* Abusing the vendor field is a hack, upstream does not like this either.

* Ignoring the bi-directional mapping will be a PITA, as it implies
  not being able to automatically bootstrap a new port or dpkg package
  builds. The former is annoying but not done frequently, the latter
  is going to be a huge problem for non-dpkg based distributions.
  This also causes other problems on triplet selection in build systems.
  It ends up being a matter of off-loading the knowledge of the architecture
  and build system from the dpkg/gcc combo to the porters/maintainers,
  which seems rather unappealing.

* We already have GNU triplets for the powerpcspe and lpia architectures,
  although those do not need upstream support as are handled transparently
  by the -gnu* patterns.

* But the GNU triplets have always encoded the ABI, it's even explicit in
  some cases, for example with EABI, packaging format (elf, aout, ecoff,
  etc) or stuff like powerpc-*-eabispe* or powerpc-*-eabialtivec*.

* ABI changing options -m128bit-long-double, -fpcc-struct-return, etc

* What about being able to co-install different default (cross-)compilers for
  different ABIs sharing the same triplet? While we might avoid the problem
  for co-installing multiarch libraries, we will not be able to for the
  cross-compilers.

* Our current assumptions about unique triplets also break for our
  packages supporting cross-building, the following code does not hold
  true any longer:

  ,---
  ifeq ($(DEB_BUILD_GNU_TYPE), $(DEB_HOST_GNU_TYPE))
    conf_gnu_type += --build $(DEB_HOST_GNU_TYPE)
  else
    conf_gnu_type += --build $(DEB_BUILD_GNU_TYPE) --host $(DEB_HOST_GNU_TYPE)
  endif
  `---

  For further details why that's needed check the autotools-dev section
  “Calling GNU configure properly” in its README.Debian.

* Other packaging systems seem to not have made the same assumption about
  the GNU triplet as dpkg does. Gentoo's portage at least relies on the
  GNU triplet being unique per different architecture as it uses it on the
  path, it might not require bi-directional mapping though. rpm and conary
  for example use uname which is not enough by itself, and quite
  unsatisfactory, which probably has not presented as a problem for them
  as they do not have the amount of supported architectures as Debian does,
  neither do they seem to have the same amount of mixes/convinations we do.

* i386 triplets

  Due to #611741, it has already crossed my mind that we should switch our
  i386 arch to use the base cpu name for the triplet (i386-linux-gnu) in
  the same way we use it in other architectures, like on arm where we use
  arm-linux-gnu instead of something like armv5tel-linux-gnueabi, for
  example. (I mentioned this already to Steve on some private conversation.)

* One can already easily create ABI incompatible object files, w/o the need
  for a new compiler. And I think this is fine, but if one would want to
  create a new toolchain with those defaults then that still require a new
  triplet (variant). The current upstream matchers should allow this just
  fine (except for the EABI case, patch attached).


So while some of these concerns _could_ be considered corner-cases, there
are several of them, which work just fine with our current assumptions,
and breaking them seems way worse as it adds future confusion and
complexity, instead of the extremely "simple" solution of getting
new triplet.

Possible solutions
------------------

* Use additional pre-processor macros for armhf, inccurs an additional
  pre-processor call for the arm arches. It implies the Debian architectures
  cannot be fully retrieved w/o a cross-compiler for each of the architectures
  lacking information, which means we lose the univeral bi-directional
  mapping, might as well switch dpkg-architecture to use internally the
  multiarch tuples as debtriplets (or the other way around).

* Abuse the vendor field (current solution?). Ugh.

* Just use our own triplet arm-*-*-gnueabihf or whatever in a similar vein
  as our own -gnulp and -gnuspe, would not need to be explicitly supported
  by gcc upstream. Build systems might be matching against stuff like
  arm*-*-linux-gnueabi, so it might require changes to match on -gnueabi*
  instead, but is more immediate, and not like propagating config.guess
  all over the place.

  PoC patch attached.

* ...
