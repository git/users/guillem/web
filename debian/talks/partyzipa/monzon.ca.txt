teorica
-------

* perque?
 - propostes anteriors (dpkg, libc)
 - opcio actual (glibc)
   + portabilitat
   + no dependecia de linux
   + moure coses userspace (codi mes generic)
   + concepte (kernel != systema complert) [kfreebsd]
   + rellevancia dins debian

* feina feta
 - glibc (Bruno || Momchil) (no acabat)
 - bootstrap Debian
 - sistema arrancable porc
 - sistema arrancable refinat
 - toolchain (no acabat)
 - autotools (propagacio, etc)
 - portabilitat utils adoptades (not kernel tied)
 - X (xserver)

* futur
 - que esperem
 - idea de kernel com a un component mes ...
 - kfreebsd compilable sota glibc
 - portabilitat/reescriptura kernel tied utils
 - portabilitat massiva (tot debian)
 - X (alternatives a XFree86)

practica
--------

* instalacio (traducional :)
* commands basics de debian
* arrancar X
(* port d'una applicacion a l'aztar)

