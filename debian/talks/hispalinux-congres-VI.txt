From: Guillem Jover <guillem@hadrons.org>
Subject: Resum VI Congres Hispalinux

Hola,

El Victor Barba i jo de GNUAB varem anar a l'ultim congres de
Hispalinux. Em centrare en el que ens toca mes directament dins la
comunitat Universitara relacionada amb el soft lliure.

Cesar Brod va exposar en una conferencia la seva experiencia en l'us
de soft lliure en la universitat d'Univates on han migrat gaire be
tot el seu soft administratiu a soft lliure.

Fan servir un euqivalent a gforge/sourceforge tot i que esta en
Portugues, no es dificil d'entendre ;> :

        <http://codigolivre.org.br/>

Alguns dels projectes que desenvolupen i fan servir:

  SAGU - Sistema Aberto de Gesta~o Unificada
    <http://sagu.codigolivre.org.br/>

  Qualitas - Sistema de Controle de Documentos para Gesta~o da Qualidade
    <http://qualitas.codigolivre.org.br/>

  MIOLO - Object Oriented Framework for Development of Systems in PHP
    <http://miolo.codigolivre.org.br/>

  PILA - Sistema que faz o controle de uma Feira do Livro
    <http://pila.codigolivre.org.br/>

  GNUData
    <http://gnudata.codigolivre.org.br/>

  GNUTeca - Sistema aberto de gesta~o de bibliotecas
    <http://gnuteca.codigolivre.org.br/>

  Rau-tu - Base de conhocimento
    <http://rau-tu.univates.br/>
    <http://www.rau-tu.unicamp.br/>

  Agata Report
    <http://agata.codigolivre.org.br/>


Varem assitir a dues taules rodones relacionades amb les Universitats i
el software lliure, es van comentar coses molt interessants, com la
d'unir esforços i crear un gforge/sourceforge a nivell nacional
per compartir recursos, ja que unes quantes Universitats podrien unir
esforços i crear el seu propi soft de gestio. Tambe podria servir per
allotjar qualsevol projecte d'investigacio que es desenvoluipes a la
Universitat.

Varem estar xarrant amb gent de l'Anillo Universitario de Software
Libre <http://afsl.adi.uam.es/anilloi/>, per tal de coordinar esforços.

salut,
guillem
