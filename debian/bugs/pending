Bug List:
---------

 * namespace violations:
   + static libraries names (incorrect):
      - libfoo_s.a (libyajl_s.a → libyajl-dev)
   + package names (incorrect, missing soversion, etc)
     - lib*-dbg (missing soversion)
     - coinor-libcbc0
     - coinor-libcgl0 (coinor-lib*)
     - libbash (namespace takeover)
     - liblife
     - libss7-1 vs libss2 (collision)
     - odbcinst1debian2
   + package names too generic
     - alarm-clock-applet
     - config-manager
     - libbasicusageenvironment1
     - libasm0, libui0
     - libgroupsock8
     - liblsofui4
     - libmaildir4
     - libmessage-filters0d
     - libparserutils0
     - libplayer1
     - libprocesscore4
     - libprocessui4
     - libstreams0v5
     - libstreamanalyzer0v5
     - libtaskmanager4
     - libusageenvironment3
     - mail-notification
     - perftest
     - software-center
     - staticsite
     - window-size (node.js specific)
     - sync-ui
     - system-config-cluster
     - tablelist
     - tvflash
     - usbmuxd (iPhone/iPod specific)

 * libopensurgsim
   - extremely generic shared libraries.
   - no SONAME encoded in package name.

 * plugins package:
   - libgemrb

 * utils/tools package:
   - libnxt
   - libuser

 * shared lib metapackages:
   - libruby
   - libpbseq
   - libotb

 * conglomerate packages (mixed shared libs + bins + includes):
   - libcwd
   - pixie
   - varnish

 * libphutil: provides no shared library, rename to -sources.

 * armada-backlight: use linux backlight subsystem instead of raw io.

 * xtitle: should not exist (https://tldp.org/HOWTO/Xterm-Title.html).

 * xbel-utils: package removed as python-xml is provided by python.

 * read-edid: wrong portability

 * wrong description:
   - filezilla (bulleted description, locales (split, names, location))
   - html-xml-utils (bulleted description)
   - psi-plus (bulleted description)

 * inappropriate changelog entries/closures:
   - oprofile 0.9.3-2
   - xfsprogs 2.9.4-1
   - mc 1:4.6.2~pre1-1
   - lynx-cur 2.8.7dev12-1

 * instances of libx86:
   - hwinfo

 * mutt
   - broken new lines with QP "=EF=BB=BF"
 * aptitude
   - shows Essential on apt?!.
 * ogamesim
   - general package review.
 * timidity
   - fucked clean target.
 * debbugs
   - duped usertags
   - usertags should apply to all merged bugs?
 * live-helper
   - embedded doc-debian and sbm image.

Old ones
--------

 * ddp
   - Securing Debian Manual:
     + section 4.17.4: linux specific bind
 * mplayer
   - libdha soname problems
 * xdm && gdm
   - inittab with 8 virtual terminals && xdm by default vt7 == xdm hangs
 * wine
   - wrong keymap when invoking some games:
     starcraft
     simcity3000
 * mcedit
   - invoking mcedit w/o file argument don't exec external commands
   - external call uses $2 = /dev/null
 * agetty (util-linux) #110564
   - writing non-us-ascii chars (ç,ó,ä), in the login prompt followed by
     a backspace, prints the chars "\210\240\210".

     problem with a wrongly detected parity.

     notes:	not affected		    ->	mingetty
     						rungetty
     		not supporting non-us chars ->	fbgetty
     						gettyps (bad behavior)
 * gettyps
   - not 8-bit clean. prints the masked 7-bit char of an 8-bit input.
 * apt-proxy
   - bad comparison between versions as '%3a' != ':'
   - does not remove damaged files
   - no supports symlinks between package files
 * kernel 2.4.x
   - startx; modprobe tdfxfb
     cursor is set very big, 8 columns x 4 rows.
 * urlview:
   - not handles quoted-printable text. '=3D' -> '='
