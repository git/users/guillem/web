#include <sys/types.h>
#include <sys/fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <stdlib.h>

int main(int argc, char **argv)
{
	off64_t offs;
	int err, fd;

	if ( argc < 2 )
		return 1;
	offs = strtoll(argv[1], 0, 10);
	printf("offset: %lli\n", offs);

	fd = open64("/dev/hd0", O_RDONLY);
	err = lseek64(fd, offs, SEEK_SET);

	printf("error: %s\n", strerror(errno));

	return 0;
}
