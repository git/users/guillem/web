#!/bin/sh

lists=/var/lib/apt/lists/*_sid_main_*_Translation-en

total=`grep "^ *[-+\*o] " $lists | wc -l`

for tag in "\*" "-" "+" "o"; do
  items=`grep "^ *$tag " $lists | wc -l`
  percent=`echo "scale=4; $items / $total * 100" | bc`
  echo "Tag $tag was used $items times ($percent%)"
done
