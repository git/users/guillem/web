#!/bin/bash

# © (<- thats a unicode copyright sign :-) ) Joachim Breitner
# nomeata@debian.org
# This is under the public domain. Do with it whatever you want.
# No blaming me, ok!?!

# This little script prints those keys in yourkeyring that are 
# fully trusted but not signed by you, which means they got their
# trust by being singed by people that you put ownertrust in.

# This was a quick hack, improvement ideas are welcome.

# Enter your ow key id here:
ownid=4743206C

for key in $(gpg --list-keys --with-colons|grep ^pub:f|cut -d: -f 5)
do
  gpg --list-sigs $key |grep -q -i $ownid || echo $key
done | xargs --no-run-if-empty -n 3 gpg --list-keys|grep ^pub
